# Translations
* [en-US](https://gitlab.com/comphack/translation/-/tree/en-US) - American English

# Notable Forks
These forks are run by seperate teams but they are worth checking out:
* Be the first to add your fork here!

# Adding a Translation
Fork the project and setup your initial translation. Once ready, please contact us by posting an [issue](https://gitlab.com/comphack/translation/-/issues/new). Include the link to the fork and if you would like the fork mentioned or a new branch on the main translation repository.
